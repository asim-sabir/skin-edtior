<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
		<title>Welcome to the demo company</title>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="assets/skin/css/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="assets/skin/css/responsive.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="assets/skin/css/jquery-ui.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="assets/skin/css/jquery.mCustomScrollbar.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="assets/skin/css/colpick.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="assets/skin/css/custome_css.css" media="screen" />
		<link id="hover-css" rel="stylesheet" type="text/css" href="assets/skin/css/hover.css" media="screen" />
		<!--[if lt IE 9 ]> <html class="ie"> <![endif]-->
		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<![endif]-->
		<script type="text/javascript" src="assets/skin/js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="assets/skin/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="assets/skin/js/jquery.bxslider.js"></script>

	</head>
	<body>
		<!--Page Wrapper Start-->
		<div id="wrapper" class="product-section">
			<!--Header Section Start-->
			<header id="header">
				<div class="view-sm mobile-nav clearfix">
					<a href="javascript:void(0);" class="fa fa-navicon nav-toggle hover_effect22"> </a>
					<a href="javascript:void(0);" class="fa fa-share-alt"></a>
				</div>
			</header>
			<!--Header Section End-->
			<!--Content Area Start-->
			<div id="content">
				<div class="pre-tool-panel" id="model_panel" style="display: none">
					<header class="heading">
						<h3>
							<span>Choose your device model</span>
						</h3>
						<a href="javascript:void(0);" id="model_bck" class="btn back prod_bck">Back</a>
					</header>
					<div class="model-table-outer">
						<div class="model-table">
							<section class="model-wrapper choose-devices">
								<ul id="modelList" class="owl-carousel"></ul>
							</section>
						</div>
					</div>
					<section class="side-nav slider">
						<a href="javascript:void(0);" class="prev slider-btn"></a>
						<a href="javascript:void(0);" class="active">1/1</a>
						<a href="javascript:void(0);" class="next slider-btn"></a>
					</section>
				</div>
				<div class="pre-tool-panel" id="brand_panel">
					<a href="javascript:void(0);" id="brand_bck"  class="btn back prod_bck">Back</a>
					<header class="heading">
						<h3><span>Choose your device brand</span></h3>
					</header>
					<div class="model-table-outer">
						<div class="model-table">
							<section class="model-wrapper  choose-devices">
								<ul id="brandList" class="owl-carousel"></ul>
							</section>
						</div>
					</div>
					<section class="side-nav slider">
						<a href="javascript:void(0);" class="prev slider-btn"></a>
						<a href="javascript:void(0);" class="active">1/1</a>
						<a href="javascript:void(0);" class="next slider-btn"></a>
					</section>
				</div>
				<div class="pre-tool-panel" id="product_panel" style="display: none">
					<header class="heading">
						<h3>
							<span>Choose your device product</span>
						</h3>
						<a class="btn product-cancel" href="javascript:void(0);">cancel</a>
					</header>
					<div class="model-table-outer">
						<div class="model-table">
							<section id="devices-slider" class="model-wrapper devices">
								<ul id="productList" class="owl-carousel"></ul>
								<section class="side-nav slider">
									<a href="javascript:void(0);" class="prev slider-btn"></a>
									<a href="javascript:void(0);" class="active">1/1</a>
									<a href="javascript:void(0);" class="next slider-btn"></a>
								</section>
							</section>
						</div>
					</div>
				</div>
				<div class="model-table-outer" id="tool_panel" style="display: none">
					<!-- Tool Navigation Pallet -->
					<section class="tool-pallet">
						<span class="title"> menu </span>
						<ul>
							<li class="product-tool menuTab" alt="product_tab">
								<i class="layer-effect"></i>
								<a href="javascript:void(0);">
									<span class="svg-wrap mobile">
										<img src="assets/skin/svg/mobile-phone.svg" class="svg" alt="" />
									</span>
									<span class="label">Products</span>
								</a>
								<i class="effect"> </i>
							</li>
							<li class="menuTab upload_tabs" alt="upload_tab">
								<i class="layer-effect"></i>
								<i class="upload-tool"></i>
								<a href="javascript:void(0);"> <span class="svg-wrap upload"> <img src="assets/skin/svg/upload.svg" class="svg" alt="" /> </span> <span class="label"> Upload Image </span> </a>
								<i class="effect"> </i>
							</li>
							<li class="write-tool menuTab" alt="text_tab">
								<i class="layer-effect"></i>
								<a href="javascript:void(0);"> <span class="svg-wrap text"> <img src="assets/skin/svg/type.svg" class="svg" alt="" /> </span> <span class="label"> Add Text </span> </a>
								<i class="effect"> </i>
							</li>
							<li class="preview-tool menuTab" alt="preview_tab">
								<i class="layer-effect"></i>
								<a href="javascript:void(0);"> <span class="svg-wrap eye"> <img src="assets/skin/svg/eye.svg" class="svg" alt="" /> </span> <span class="label"> Preview </span> </a>
								<i class="effect"> </i>
							</li>

							<li class="menuTab cart_tab" alt="cart_tab">
								<i class="layer-effect"></i>
								<i class="cart-tool"></i>
								<a href="javascript:void(0);"> <span class="svg-wrap cart"> <img src="assets/skin/svg/shopping-cart.svg" class="svg" alt="" /> </span> <span class="label"> Add to Cart </span> </a>
								<i class="effect"> </i>
							</li>

						</ul>
					</section>
					<!-- Tool Navigation Pallet  -->
					<section class="model-container">
						<!-- 	How to use pallet -->
						<section class="use-steps-section" style="display: none"  >
							<header class="clearfix">
								<span  class="i-image  svg-img"> How to use? <span class="svg-wrap info"><img src="assets/skin/svg/info.svg" class="svg" alt="" /></span> </span>
							</header>
							<div class="step-pallet">
								<span class="border-layer"> </span>
								<ul>
									<li>
										<strong class="title-text" > How to use the
										Customizer... </strong>

										<i class="use-effect"></i>
									</li>

									<li>

										<div class="step clearfix">
											<span class="svg-wrap mobile"> <img class="svg" src="assets/skin/svg/mobile-phone.svg" /> </span>
											<div class="step-block">
												<span class="step-no">Step 1</span>
												<a href="javascript:void(0);">Select device</a>
											</div>

										</div>
										<i class="use-effect"></i>
									</li>

									<li>

										<div class="step clearfix">
											<span class="svg-wrap photo"> <img class="svg" src="assets/skin/svg/photo.svg" /> </span>
											<div class="step-block">
												<span class="step-no">Step 2</span>
												<a href="javascript:void(0);">Upload your images or
												choose from our gallery</a>
											</div>

										</div>
										<i class="use-effect"></i>
									</li>

									<li>
										<div class="step clearfix">
											<span class="svg-wrap photo"> <img class="svg" src="assets/skin/svg/shopping-cart.svg" /> </span>
											<div class="step-block">
												<span class="step-no">Step 3</span>
												<a href="javascript:void(0);">Preview your design and proceed to the checkout</a>
											</div>

										</div>
									</li>

								</ul>
							</div>

						</section>
						<!-- 	How to use pallet -->

						<!-- 	main tab	-->
						<div class="model-tab no-heading" >

							<div class="product-wrapper" style="display:none">

								<div class="display-banner" >
									<span class="display-front"><img src="assets/skin/images/phone-back.jpg" alt="" /></span>
									<span class="display-back"><img src="assets/skin/images/phone-front.jpg" alt="" /></span>
								</div>
								<div class="check-on-product">
									<div class="blank-div"></div>
									<a href="javascript:void(0);" class="btn buy">Buy Now</a>
									<a href="javascript:void(0);" class="btn">Go Back</a>

								</div>
							</div>
						</div>
						<!-- 	main tab	-->

						<!---- Browse tab	-------->
						<div class="model-tab" id="browse_panel" style="display: none">
							<header class="heading">
								<h3><span>where do you want to get your Images?</span></h3><a href="javascript:void(0);" class="btn browse_bck cancel back">cancel</a>
							</header>

							<div class="browse-options">
								<ul id="imageBrowse">
									<li id="uploader">
										<span class="svg-wrap arrow-right"> <img class="svg" src="assets/skin/svg/arrow-right.svg" alt="" /> </span>
										<a id="upload_file" class="plupload_button plupload_add main-text" onClick="return false;"   href="javascript:void(0);"> <span id="MyCom">My Computer</span> <small id="UpMyUpl">Upload your own image</small> </a><a href="javascript:void(0);" onClick="return false;"  class="plupload_button plupload_start" style="display:none;">Start upload</a>

									</li>

									<li id="imgGallery">
										<span class="svg-wrap arrow-right"> <img class="svg" src="assets/skin/svg/arrow-right.svg" alt="" /> </span>

										<div class="main-text">
											<h4>Browse Our Images Library</h4>
											<span class="caption">Choose from our geart designs</span>
										</div>

									</li>

									<li class="find-url">
										<span class="svg-wrap arrow-right"> <img class="svg" src="assets/skin/svg/arrow-right.svg" alt="" /> </span>

										<div class="main-text">
											<h4>Find Photo Online (URL)</h4>
										</div>
									</li>
								</ul>

							</div>

						</div>
						<!---- Browse tab	-------->

						<!-- Image Library ---->

						<div class="model-tab library-preview" style="display: none"  >
							<header class="heading">
								<h3><span>Image Library</span></h3><a href="javascript:void(0);" class="btn img_lib_bck cancel back">Cancel</a>
							</header>
							<div class="categoris-box">
								<div class="filter categoris">
									<span class="current-select">Choose Category</span>
									<div class="custom-select " id="clipCategory">
										<span class="select-box"> Arial Black </span>

										<ul class="dropdown">
											
										</ul>

									</div>
								</div>

								<div class="filter sub-categoris">
									<span class="current-select">Choose sub-Category</span>
									<div class="custom-select " id="clipSubcategory">
										<span class="select-box"> Arial Black </span>

										<ul class="dropdown">
											
										</ul>

									</div>
								</div>

							</div>
							<div class="image-library clearfix">

								<ul id="clipList">
									
								</ul>
							</div>

						</div>
						<!-- Image Library ---->

						<!--   Product Type  -->

						<div class="model-tab" id="upload_panel" style="display: none">
							<section class="device-view">
								<div class="select-view">
									<section class="device-view-pallet">
										<h5>Device View</h5>
										<div class="select-device-view"></div>
									</section>
									<div class="uploaded-images-wrapper">

										<div class="uploaded-images">
											<h5>Your Images</h5>

											<ul id="uploadIMg">

											</ul>
										</div>
										<div class="apply-effect only-lg-view disable">
											<h5><span class="effect-svg"><img src="assets/skin/svg/effect.svg" class="svg" alt="" /></span> Apply Effect </h5>
										</div>
									</div>
									<!--      Add Text    -->
									<div class="add-text hide">
										<h5>Add Text </h5>
										<form action="LivePreview.html#" method="post">
											<section class="form-box">
												<textarea id="textP" rows="1" cols="1" placeholder="ADD TEXT HERE"></textarea>
												<div class="text-tool">
													<span class="tool-btn bold active"> b </span>
													<span class="tool-btn italic"> i </span>
												</div>
												<input type="button" id="updateText" value="Update Text" class="btn disable">
												<input type="button" id="addText" value="Add Text" class="btn">
											</section>

											<section class="form-box">
												<div class="custom-select " id="fontList">
													<span class="select-box font"> Arial Black </span>

													<ul class="dropdown fontDrop">
														
													</ul>

												</div>

												<div id="colorList" class="custom-select">
													<span class="select-box color"> <i class="color-pallet"></i> Black </span>

													<ul class="dropdown  colorDrop">
														
													</ul>

												</div>
											</section>
										</form>
										<span class="closeblock delete-btn">x</span>
									</div>

								</div>
							</section>

						</div>

						<!--   Product Type  -->

						<!--      Preview       -->
						<div class="model-tab" id="preview_panel" style="display: none">

							<section class="preview-panel">

								<div id="previewPanel" class="preview-section">
									<span class="preview-front"><img src="assets/skin/images/phone-front.jpg" alt="" /> <span class="preview-name">FRONT</span> </span>
									<span class="preview-back"><img src="assets/skin/images/phone-back.jpg" alt="" /><span class="preview-name">BACK</span></span>
								</div>
								<div class="nav-btn-panel clearfix">
									<span id="prev_back" class="nav-btn back"><a href="javascript:void(0);" class="new-btn"> <i class="fa fa-long-arrow-left"></i> <span class="back-text">Go back </span></a></span>
									<span class="nav-btn buy-now"> <a href="javascript:void(0);" class="new-btn"> Buy now <span class="buy-arrow"> <i class="fa fa-caret-right"></i> </span> </a> <span class="price"> Rs. 399.00 </span> </span>
								</div>

							</section>
						</div>

						<!--      Add Text    -->

						<!-- Choose Device -->

						<div class="model-tab"  style="display:none">

							<section class="device-pallet">
								<ul class="product-slider">
									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												Phone Skins
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												Tablets
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												Laptops
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												Game Devices
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												iPods
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												Hard Case
											</figcaption>

										</figure>
									</li>

								</ul>

							</section>

						</div>

						<!-- Choose Device -->

						<!-- Choose Brand -->

						<div class="model-tab" style="display:none">

							<section class="device-pallet brand">

								<header class="heading">
									<h3>Choose your device brand</h3>
								</header>
								<ul class="product-slider">
									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												Apple
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												Samsung
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												Blackberry
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												Nokia
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												HTC
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												Amazone
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												Apple
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												Samsung
											</figcaption>

										</figure>
									</li>

								</ul>

							</section>

						</div>

						<!-- Choose Brand -->

						<!-- Choose Model -->
						<div class="model-tab" style="display:none">

							<section class="device-pallet brand model">
								<header class="heading">
									<h3>Choose your device model</h3>
								</header>

								<ul class="product-slider">
									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												iPhone 4
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												iPhone 4s
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												iPhone 5
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												iPhone 3
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												iPhone 4
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												iPhone 4s
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												iPhone 5
											</figcaption>

										</figure>
									</li>

									<li>
										<figure>
											<div class="product-wrap">
												<span class="product-img-wrap"></span>
											</div>
											<figcaption>
												iPhone 3
											</figcaption>

										</figure>
									</li>

								</ul>

							</section>

						</div>
						<!-- Choose Model -->

						<!-- Product Type -->

						<div class="model-tab"  style="display: none" >

							<section class="device-accessories device-pallet">
								<header class="heading">
									<h3>Choose product type for iphone</h3>
								</header>
								<ul class="product-slider">

									<li>

										<figure>
											<strong class="product-caption"> Skin </strong>
											<small class="price">Starting at <span>£ 7.99</span></small>
											<div class="product-wrap">
												<span class="product-img-wrap"><img src="assets/skin/images/product-type-1.jpg" alt="" /></span>
											</div>

											<a href="javascript:void(0);" class="btn"> Start Now </a>

										</figure>

									</li>

									<li>

										<figure>
											<strong class="product-caption"> Skin </strong>
											<small class="price">Starting at <span>£ 7.99</span></small>
											<div class="product-wrap">
												<span class="product-img-wrap"><img src="assets/skin/images/product-type-2.jpg" alt="" /></span>
											</div>

											<a href="javascript:void(0);" class="btn"> Start Now </a>

										</figure>

									</li>

									<li>

										<figure>
											<strong class="product-caption"> Skin </strong>
											<small class="price">Starting at <span>£ 7.99</span></small>
											<div class="product-wrap">
												<span class="product-img-wrap"><img src="assets/skin/images/product-type-3.jpg" alt="" /></span>
											</div>

											<a href="javascript:void(0);" class="btn"> Start Now </a>

										</figure>

									</li>
								</ul>

							</section>

						</div>
						<!-- Product Type -->

					</section>
					<section class="device-section">
						<div class="canvas-section"><img src="assets/skin/images/phone-back.jpg" alt="" />
						</div>
						<canvas id="canvas"></canvas>
					</section>
				</div>

			</div>
			<!--Content Area End-->

			<!--Footer Section Start-->
			<footer id="footer" class="disable-footer">

				<span class="tool-button"> <span class="tooloptions"> Slide Up </span> </span>

				<section class="footer-toolbar hide-panel">
					<ul>
						<li class="scale-section">

							<section class="tool-wrap">
								<div class="tool-img scale-range">
									<strong class="panel-title">scale</strong>
									<span class="main-tool">
										<div id="scaleSlide" class="slider-range"><img class="scale" src="assets/skin/images/scale-tool.png" alt="" />
										</div> <span class="lock-img"> <i class="lock lock-tool"> <img src="assets/skin/svg/locked.svg" alt="" class="svg" /> </i> <i class="un-lock lock-tool"> <img src="assets/skin/svg/locked.svg" alt="" class="svg" /> </i> </span> </span>

									<i class="decrease value-option"> - </i>
									<i class="increase value-option"> + </i>

								</div>

								<div class="tool-img transparency-range">
									<strong class="panel-title">Transparency</strong>
									<span class="main-tool tranparency"> <i class="decrease">0</i>
										<div id="tran_Slide" class="slider-range">
											<img src="assets/skin/images/tranparency-tool.png" alt="" />

										</div> <i class="increase">100</i> <!-- <div id="slider"></div>  --> </span>

								</div>

							</section>
						</li>
						<li class="rotate-section">
							<section class="tool-wrap rotate">
								<div class="tool-img ">
									<strong class="panel-title">Rotate</strong>
									<div class="rotateBox">
										<div class="rotation">
											<input id="rotationVal" disabled="" type="text" value="0"/>
											<span class="rotater" id="rotateThumb"></span>
										</div>
									</div>

								</div>
							</section>

						</li>
						<li class="undo-section">

							<section class="tool-wrap">
								<div class="tool-img ">
									<strong class="panel-title">Undo/Redo</strong>
									<span class="main-tool undo-redo"> <a href="javascript:void(0);" class="undo"> <img src="assets/skin/svg/undo.svg" alt="" class="svg" /> </a> <a href="javascript:void(0);" class="redo"> <img src="assets/skin/svg/redo.svg" alt="" class="svg" /> </a> </span>

								</div>

							</section>

						</li>
						<li class="background-section">
							<section class="tool-wrap background">
								<div class="tool-img ">
									<strong class="panel-title only-lg-view">Background Color</strong>
									<strong class="panel-title view-sm">Bg Color</strong>
									<span class="main-tool"> <span id="canPicker" class="bg-pallet"> <img class="only-lg-view" src="assets/skin/images/bg-color.png" alt="" /> <img class="view-sm svg palette" src="assets/skin/svg/palette.svg" alt="" /> </span> </span>

								</div>

							</section>
						</li>
						<li class="layer-section">
							<section class="tool-wrap layer">
								<div class="tool-img ">
									<strong class="panel-title">Layer order</strong>
									<span class="main-tool layer-order-wrap"> <a href="javascript:void(0);" alt="layerUp" class="layer-order  one"><img src="assets/skin/svg/layer_up.svg" class="svg" alt="" /></a> <a href="javascript:void(0);" alt="layerDown" class="layer-order  two"><img src="assets/skin/svg/layer_down.svg"  class="svg" alt="" /></a> </span>

								</div>

							</section>
						</li>
						<li class="delete-section">
							<section class="tool-wrap delete">
								<div class="tool-img ">
									<strong class="panel-title">delete</strong>
									<span class="main-tool" id="deleteObj"> <span class="trash" title="Remove"> <i class="recycle"></i> <span class="lid"></span><span class="can"></span> </span> </span>

								</div>

							</section>

						</li>
						<li class="share">

							<section class="tool-wrap">
								<div class="tool-img ">
									<strong class="panel-title">share it</strong>
									<div class="social-share">
										<ul>
											<li data-value="facebook">
												<a href="javascript:void(0);" class="fa fa-facebook"></a>
											</li>
											<li data-value="email">
												<a href="javascript:void(0);" class="fa fa-envelope"></a>
											</li>

											<li data-value="twitter">
												<a href="javascript:void(0);" class="fa fa-twitter"></a>
											</li>
										</ul>
									</div>

								</div>

							</section>

						</li>

					</ul>
				</section>

			</footer>
			<!--Footer Section End-->

			<!-- Effect Popup -->
			<div id="effectDrag" class="effect-panel model-popup">
				<div class="popup-wrap">
					<header>
						<h4> Image Adjust </h4>
						<a href="javascript:void(0);"  class="popup-close" >X</a>
					</header>

					<section class="apply-effect-panel clearfix">

						<div class="panel-wrapper clearfix">

							<div class="effect-tool">
								<div class="effect-tool-list">
									<a href="javascript:;" class="effect-btn active">Effects</a>
									<a href="javascript:;" class="adjustment-btn">Adjustments</a>
								</div>

								<section class="tab-panel-wrapper">
									<!--Tab1 -->
									<div class="effect-tab">

										<section class="effect-tab-wrapper scroll browse-options mCustomScrollbar">
											<ul id='effect-list'>
												<li alt="no-effect" class="noEffectTab active-effect">
													<span class="effect-icon"> <i class="fa fa-user"></i> <small class="fa fa-ban"></small> </span>
													<div class="main-text">
														<h5>No Effect</h5>
														<span class="caption">Clear all effects</span>
													</div>
												</li>

												<li class="effectTab" alt="sharpen">
													<span class="effect-icon"> <i class="fa fa-user"></i> <small class="sharpen"> <img src="assets/skin/svg/sharpen.svg" alt="" class="svg"  /> </small> </span>
													<div class="main-text">
														<h5>Sharpen </h5>
														<span class="caption">Getting better now</span>
													</div>
												</li>

												<li class="effectTab" alt="black-n-white">
													<span class="effect-icon"> <i class="fa fa-user"></i> <small> <i class="fa fa-adjust"></i> </small> </span>
													<div class="main-text">
														<h5>Black and white </h5>
														<span class="caption">Try the classic touch</span>
													</div>
												</li>

												<li class="effectTab" alt="sepia">
													<span class="effect-icon"> <i class="fa fa-user"></i> <small class="sepia-icon"> </small> </span>
													<div class="main-text">
														<h5>Sepia </h5>
														<span class="caption">Back to vintage era</span>
													</div>
												</li>
											</ul>

										</section>
									</div>
									<!--Tab1 -->

									<!--Tab2 -->
									<div class="effect-tab" style="display: none">
										<div class="disable-tab"></div>
										<section class="effect-tab-wrapper scroll adjust-tab browse-options mCustomScrollbar">
											<ul>
												<li>
													<span class="effect-icon"> <i class="fa fa-user"></i> <small class="fa fa-ban"></small> </span>
													<div class="main-text">
														<h5>No Effect</h5>
														<span class="caption">Clear all Adjustment</span>
													</div>
												</li>

												<li>
													<span class="effect-icon"> <i class="fa fa-user"></i> <small class="fa fa-magic"></small> </span>
													<div class="main-text">
														<h5>Auto adjust photo</h5>
														<span class="caption">Magic on photo</span>
													</div>
												</li>

												<li>
													<span class="effect-icon"> <i class="fa fa-user"></i> <small class="fa fa-adjust"></small> </span>
													<div class="main-text slider-wrap">
														<h5>Contrast</h5>

														<div class="meter-slider">
															<i class="value-option"> -</i>
															<span class="meter"> <span class="meter-bar"> <img class="svg" src="assets/skin/svg/slider_bar.svg" alt="" /> </span> </span>

															<i class="value-option"> +</i>
														</div>

													</div>
												</li>

												<li>
													<span class="effect-icon"> <i class="fa fa-user"></i> <small class="hue-icon"> <img src="assets/skin/images/hue.png" alt="" /> </small> </span>
													<div class="main-text slider-wrap">
														<h5>Hue</h5>

														<div class="meter-slider">
															<i class="value-option"> -</i>
															<span class="meter"> <span class="meter-bar"> <img class="svg" src="assets/skin/svg/slider_bar.svg" alt="" /> </span> </span>

															<i class="value-option"> +</i>
														</div>

													</div>
												</li>

											</ul>

										</section>

									</div>
									<!--Tab2 -->

								</section>

							</div>

						</div>

					</section>
				</div>
			</div>
			<!-- Effect Popup -->

			<!-- Help videos -->
			<div class="help-video model-popup">
				<div class="popup-wrap">
					<header>
						<h4> Help Videos </h4>
						<a class="popup-close-help" href="javascript:void(0);">X</a>
					</header>
					<div class="video-gallery">
						<section id="video-wrap">
							
<iframe width="560" height="315" src="https://www.youtube.com/embed/BMaCJFG4Hz4" frameborder="0" allowfullscreen></iframe>

						</section>

					</div>

					<span class="feedback"> If you need help on a subject not covered here please mail us at <a href="javascript:void(0);">admin@theemon.com </a> </span>

				</div>
			</div>
			<!-- Help videos -->

			<!-- Start customization -->
			<div class="model-popup alert-box custom">
				<div class="popup-wrap">
					<header>
						<h4>Step 2. Start customization</h4>
						<a class="popup-close" href="javascript:void(0);">X</a>
					</header>

					<section class="alert-message">
						<p>
							You can now create your own unique
							design or you can use design from our
							gallerytoo , you can add change text.
						</p>
						<p>
							Enjoy your new design.
						</p>
					</section>
				</div>
			</div>
			<!-- Start customization -->

			<!--Url Popup -->
			<div class="model-popup url-box">
				<div class="popup-wrap">
					<header>
						<h4>Find Photo Online (URL)</h4>
						<a class="popup-close" href="javascript:void(0);">X</a>
					</header>

					<section class="alert-message">
						<input type="text" id="url_image" placeholder="example:http://www.domainname.com/imagename.jpg">
						<button id="urlUploadBtn" class="url-add btn">
							Add
						</button>
					</section>
				</div>
			</div>
			<!--Url Popup -->

			<!-- Adding Cart Alert -->
			<div class="model-popup alert-box custom">
				<div class="popup-wrap">
					<header>
						<h4>Step 2. Start customization</h4>
						<a class="popup-close" href="javascript:void(0);">X</a>
					</header>

					<section class="alert-message">
						<p>
							Are you ready to add this customised
							skin to your shopping cart?
						</p>
						<div class="btn-panel">
							<a href="javascript:void(0);" class="btn">Yes</a>

							<a href="javascript:void(0);" class="btn">cancel</a>
						</div>
					</section>
				</div>
			</div>

			<div class="overlay"></div>

		</div>

		<div class="inneroverlay hide-panel">
			<div class="load-content">
				<div class="loaderImage"></div>
				<strong class="trn" title="Just a moment" data-trn-key="Just a moment">Just a moment</strong>
				<span class="trn" title="Sending to Add to Cart page in 5 seconds" data-trn-key="Sending to Add to Cart page in 5 seconds">Sending to Add to Cart page in 5 seconds</span>
			</div>
		</div>
		<!--Page Wrapper End-->
		<script type="text/javascript" src="assets/skin/js/jquery.ui.touch-punch.min.js"></script>
		<script type="text/javascript" src="assets/skin/js/Tocca.js"></script>
		<script type="text/javascript" src="assets/skin/js/jquery.unveil.js"></script>
		<script type="text/javascript" src="assets/Library/webfont.js"></script>
		<script type="text/javascript" src="assets/skin/js/jquery.mCustomScrollbar.js"></script>
		<script type="text/javascript" src="assets/Library/model.js"></script>
		<script type="text/javascript" src="assets/Library/jquery.sparx-tool.js"></script>
		<script type="text/javascript" src="assets/Library/command.js"></script>
		<script type="text/javascript" src="assets/Library/service.js"></script>
		<script type="text/javascript" src="assets/skin/js/colpick.js"></script>
		<script type="text/javascript" src="assets/Library/controller.js"></script>
		<script type="text/javascript" src="assets/Library/plupload.full.js"></script>
		<script type="text/javascript" src="assets/Library/jquery.plupload.queue.js"></script>
		<script type="text/javascript" src="assets/Library/percentage.js"></script>
		<script type="text/javascript" src="assets/skin/js/responsive.js"></script>
		<script type="text/javascript" src="assets/Library/socialShare.js"></script>
		<script type="text/javascript" src="assets/Library/color_effect.js"></script>
		<script type="text/javascript" src="assets/skin/js/ImgZoom.js"></script>
		<script type="text/javascript" src="assets/skin/js/owl.carousel.js"></script>
		<script type="text/javascript" src="assets/skin/js/jquery.flexslider.js"></script>
		<script type="text/javascript" src="assets/skin/js/site.js"></script>
	</body>
</html>