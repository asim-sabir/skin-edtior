(function(jq) {
	jq(window).resize(function() {
		if (this.resizeTO)
			clearTimeout(this.resizeTO);
		this.resizeTO = setTimeout(function() {
			jq(this).trigger('resizeEnd');
		}, 500);
	});

	jq(window).bind('resizeEnd', function() {
		var flag = jq('#tool_panel').css('display') == 'block' ? true : false;
		if(flag){
			command.canvasLoad(service.viewData[opc_current_view]);
		}
	});
})(jQuery)
