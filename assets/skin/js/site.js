jq(document).ready(function() {
	if(opc_show_adjustment_window == false || opc_show_adjustment_window == 'false'){
		jq('.effect-tool .adjustment-btn').css('display','none');
	}
	
	isMobile = navigator.userAgent.match(/(iPhone|iPod|Android|BlackBerry|iPad|IEMobile|Opera Mini)/);
	if (isMobile) {
		jq('#hover-css').remove();
	}
	jq('.bxslider').bxSlider({
		pagerCustom : '#bx-pager'
	});

	jq(window).load(function() {
		jq('#carousel').flexslider({
			animation : "slide",
			controlNav : false,
			animationLoop : false,
			slideshow : false,
			itemWidth : 136,
			itemMargin : 5,
			asNavFor : '#slider'
		});

		jq('#slider').flexslider({
			animation : "slide",
			controlNav : false,
			animationLoop : false,
			slideshow : false,
			sync : "#carousel"

		});
	});
	//=========
	jq('.mobile-nav .fa-share-alt').one('click', function() {
		jq('.tool-img  .social-share').clone().appendTo('.mobile-nav');

	})
	jq('.mobile-nav .fa-share-alt').on('click', function() {
		jq('.social-share').slideToggle();
		if (jq('#footer').css('bottom') == '0px') {
			jq('#footer').animate({
				bottom : '-159px'
			})
			jq('.tooloptions').text("Slide Up")
			jq('.up-tools').removeClass('up-tools');
		}
		if (jq('.nav-toggle').hasClass('fa-times')) {
			jq('.tool-pallet').slideUp(300);
			jq('#content').toggleClass('tool-tab');
			jq('.nav-toggle').toggleClass('fa-navicon');
			jq('.nav-toggle').toggleClass('fa-times');
		}

	});

	jq('img.svg').each(function() {
		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		jQuery.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = jQuery(data).find('svg');

			// Add replaced image's ID to the new SVG
			if ( typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if ( typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass + ' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');

			// Replace image with new SVG
			$img.replaceWith($svg);

		}, 'xml');

	});

	// jq('.image-library li').click(function() {
	// jq('.overlay').fadeIn(300);
	// jq('.image-preview').fadeIn(350);
	// jq(".image-preview .preview").children('img').remove();
	// jq(this).children('img').clone().appendTo(".image-preview .preview");
	// })
	jq('.video-btn').on(opc_EventType, function() {
		jq('.help-video').css('visibility', 'visible');
		jq('.overlay').fadeIn(350);

	});

	jq('.popup-close').on(opc_EventType, function() {
		jq('.model-popup').fadeOut(300);
		jq('.overlay').fadeOut(350);
	});

	jq('.popup-close-help').on(opc_EventType, function() {
		jq('.model-popup.help-video').css({
			'visibility' : 'hidden',
			'display' : 'block'
		});
		jq('.overlay').fadeOut(350);
	});
	//====effect Panel Tab===============
	jq('#fontList').on(opc_EventType, function() {

		//alert(22)
		jq('#colorList').find('.colorDrop').css({
			'display' : 'none'
		});

		//jq('.fontDrop').siblings('.fontDrop').css({'display':'none'});
	});

	jq('.effect-tool-list a ').on(opc_EventType, function() {
		jq('.effect-tool-list a ').removeClass('active');
		jq(this).addClass('active');
		var ind = jq(this).index();
		jq('.tab-panel-wrapper .effect-tab').fadeOut(300);

		jq('.tab-panel-wrapper .effect-tab').eq(ind).fadeIn(300);

		jq('.effect-tab-wrapper').mCustomScrollbar("update");
	});
	//========Select dropdown================

	// jq('.find-url').click(function() {
	// jq('.url-box').fadeIn(300);
	// jq('.overlay').fadeOut(350);
	// })
	jq('.nav-toggle').on(opc_EventType, function() {
		if (jq('#tool_panel').css('display') == 'block') {
			jq('.tool-pallet').slideToggle(300);
			jq('#content').toggleClass('tool-tab');
			jq(this).toggleClass('fa-navicon');
			jq(this).toggleClass('fa-times');

			if (jq('#footer').css('bottom') == '0px') {
				jq('#footer').animate({
					bottom : '-159px'
				})
				jq('.tooloptions').text("Slide Up")
				jq('.up-tools').removeClass('up-tools');
			}
			if (jq('.social-share').slideUp().css('display') == 'block')
				jq('.social-share').slideUp();

		}

	});

	jq('.tool-pallet li').on(opc_EventType, function() {
		if (jq(window).width() < 767) {
			jq('.nav-toggle').trigger(opc_EventType);
			//jq('.tool-pallet').slideUp(300);
			jq('#content').removeClass('tool-tab');
		}

	});	
	jq('.write-tool').on(opc_EventType, function() {

		if (jq(window).width() < 767) {
			jq('.add-text').removeClass('hide');
			jq('#upload_panel').slideDown(300);
			//jq('#content').removeClass('tool-tab');
		}

	});
	jq('.uploaded-images h5').on(opc_EventType, function() {

		if (jq(window).width() < 767) {
			if (jq('#uploadIMg').find('img').length > 0) {
				jq('.uploaded-images h5').toggleClass('show-images');
				jq('#uploadIMg').slideToggle(300);
			} else {
				jq('.no-image').addClass('scale');
			}
		}

	});
	if (jq(window).width() < 767) {
		jq('.product-slider').bxSlider();
	}
	// Crose browser

	//Select Device On mobile veiw
	jq('.device-view-pallet').on(opc_EventType, function() {
		if (jq(window).width() < 767) {
			jq('.select-device-view').slideToggle();
			jq('.device-view-pallet h5').toggleClass('show-device');
		}
	});
	//====
	jq('.closeblock').on(opc_EventType, function() {
		if (jq(window).width() < 767) {
			jq('.add-text').addClass('hide');
			//jq('.device-view-pallet h5').toggleClass('show-device');
		}
	});

	jq('#modelList').on(opc_EventType, function() {

		jq('html').addClass('html-box');
	});
	jq('.tool-pallet li.product-tool').on(opc_EventType, function() {

		jq('html').removeClass('html-box');
	});
	//footer tool button
	jq('.tool-button').on(opc_EventType, function() {
		if (jq('#footer').css('bottom') == '-159px') {

			jq('#footer').animate({
				bottom : 0
			})
			jq(this).addClass('up-tools');

			jq('.tooloptions').text("Slide Down");
			if (jq('.nav-toggle').hasClass('fa-times')) {
				jq('.tool-pallet').slideUp(300);
				jq('#content').toggleClass('tool-tab');
				jq('.nav-toggle').toggleClass('fa-navicon');
				jq('.nav-toggle').toggleClass('fa-times');
			}
			if (jq('.social-share').slideUp().css('display') == 'block')
				jq('.social-share').slideUp();

		} else {
			jq('#footer').animate({
				bottom : '-159px'
			})
			jq('.tooloptions').text("Slide Up")
			jq('.up-tools').removeClass('up-tools');
		}
	});
	jq('.footer-toolbar >ul>li').append('<div class="disable-tool"></div>');

	//Devices Touch
	jq('.ui-slider-handle,.value-option,.rotater,.layer-order').bind('touchstart', function() {
		//alert(22)
		jq(this).addClass('touch_effect');
	});

	jq('.ui-slider-handle,.value-option,.rotater,.layer-order').bind('touchend', function() {
		jq(this).removeClass('touch_effect');
	});

	function css_browser_selector(u) {
		var ua = u.toLowerCase(), is = function(t) {
			return ua.indexOf(t) > -1
		}, g = 'gecko', w = 'webkit', s = 'safari', o = 'opera', m = 'mobile', h = document.documentElement, b = [(!(/opera|webtv/i.test(ua)) && /msie\s(\d)/.test(ua)) ? ('ie ie' + RegExp.$1) : is('firefox/2') ? g + ' ff2' : is('firefox/3.5') ? g + ' ff3 ff3_5' : is('firefox/3.6') ? g + ' ff3 ff3_6' : is('firefox/3') ? g + ' ff3' : is('gecko/') ? g : is('opera') ? o + (/version\/(\d+)/.test(ua) ? ' ' + o + RegExp.$1 : (/opera(\s|\/)(\d+)/.test(ua) ? ' ' + o + RegExp.$2 : '')) : is('konqueror') ? 'konqueror' : is('blackberry') ? m + ' blackberry' : is('android') ? m + ' android' : is('chrome') ? w + ' chrome' : is('iron') ? w + ' iron' : is('applewebkit/') ? w + ' ' + s + (/version\/(\d+)/.test(ua) ? ' ' + s + RegExp.$1 : '') : is('mozilla/') ? g : '', is('j2me') ? m + ' j2me' : is('iphone') ? m + ' iphone' : is('ipod') ? m + ' ipod' : is('ipad') ? m + ' ipad' : is('mac') ? 'mac' : is('darwin') ? 'mac' : is('webtv') ? 'webtv' : is('win') ? 'win' + (is('windows nt 6.0') ? ' vista' : '') : is('freebsd') ? 'freebsd' : (is('x11') || is('linux')) ? 'linux' : '', 'js'];
		c = b.join(' ');
		h.className += ' ' + c;
		return c;
	};
	css_browser_selector(navigator.userAgent);

});
