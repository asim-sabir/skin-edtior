(function(jq) {

	jq("#uploader").pluploadQueue({
		// General settings
		runtimes : 'html5,flash,silverlight,html4',
		url : opc_Upload_Imgfile_path,
		max_file_size : opc_Max_file_size,
		chunk_size : opc_Chunk_Image_Size,
		unique_names : opc_UploadImg_Unique_Names,
		multi_selection : opc_UploadImg_Multi_Selection,
		// Specify what files to browse for
		filters : [{
			title : "Image files",
			extensions : opc_Upload_File_Extension
		}],
		// Flash settings
		flash_swf_url : 'assets/Library/plupload.flash.swf',
		silverlight_xap_url : 'assets/Library/plupload.silverlight.xap'
	});

	var uploader = jq("#uploader").pluploadQueue();

	uploader.bind('FilesAdded', function(up, file, res) {
		command.showLoader();
	});

	uploader.bind('FileUploaded', function(l, d, m) {
		response = m.response;
		var n = '';
		n = response.slice(-6);
		if (n == '.Error') {
			var data = response.replace('Error', '');
			alert(data);
		} else {
						
			command.showUploadedImages("assets/" + response,'img_userUpload');
		}
	});

})(jQuery);
