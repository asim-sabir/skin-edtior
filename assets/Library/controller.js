(function(jq) {

	/*
	 Product click Event
	 */
	jq(document).on('tap', '#productList li', function() {
		service.brandManage(jq(this));
		return false;
	});

	/*
	 Brand click Event
	 */
	jq(document).on('tap', '#brandList li', function() {
		service.modelManage(jq(this));
		return false;
	});

	/*
	 Model click Event
	 */
	jq(document).on('tap', '#modelList li', function() {
		if (opc_canvas) {
			opc_canvas.backgroundColor = null;
			opc_canvas.clear().renderAll();
		}
		command.showLoader();
		service.viewLoad(jq(this));
		return false;
	});

	/*
	 Product back button Event.
	 */
	jq(document).on(opc_EventType, '.prod_bck', function() {
		var id = jq(this).attr('id');
		switch(id) {
			case 'brand_bck' :
				service.productManage();
				break;
			case 'model_bck' :
				service.brandManage();
				break;
		}
		return false;
	});

	jq(document).on(opc_EventType, '.canvas-section', function(evt) {
		if (evt.target !== this)
			return false;
		opc_canvas.deactivateAll().renderAll();
		jq('#footer').addClass('disable-footer');
		return false;
	});

	/*
	 Menu Tab click event
	 */

	jq(document).on(opc_EventType, '.menuTab', function() {
		if (!jq(this).hasClass('activeTab')) {
			jq('.device-section').removeClass('canvasText');
			jq('.menuTab').removeClass('activeTab');
			jq(this).addClass('activeTab');
			var alt = jq(this).attr('alt');
			jq('.effect-panel').fadeOut(300);
			opc_canvas.deactivateAll().renderAll();
			jq('#footer').addClass('disable-footer');
			switch(alt) {
				case 'product_tab':
					jq('.product-cancel').css('display', 'block');
					jq('.footer-toolbar').slideUp();
					jq('#tool_panel, .model-tab, .device-section').css('display', 'none');
					jq('#product_panel').css('display', 'block');
					break;
				case 'image_tab':
					command.manageInnerPanel('imagePanel');
					break;
				case 'text_tab':
					command.manageInnerPanel('textPanel');
					break;
				case 'preview_tab':
					jq('.model-tab, .device-section').css('display', 'none');
					opc_canvas.deactivateAll().renderAll();
					jq('.footer-toolbar').slideUp();
					command.showPreview();
					break;
				case 'upload_tab':

					jq('.model-tab, .device-section').css('display', 'none');
					jq('#browse_panel').css('display', 'block');
					jq('.footer-toolbar').slideUp();
					break;
				case 'cart_tab':
					command.addToCart();
					break;
			}
		}
		return false;
	});

	/*
	 Gallery image click event.
	 */

	jq(document).on(opc_EventType, '#imgGallery', function() {
		jq('.model-tab').css('display', 'none')
		jq('.library-preview').css('display', 'block');
		return false;
	});

	/*
	 Get image from url path.
	 */

	jq(document).on(opc_EventType, '.find-url', function() {

		jq('.url-box').fadeIn(300);
		jq('.overlay').fadeIn(350);
		jq('.url-box #url_image').val('');
		return false;
	});

	jq(document).on(opc_EventType, '#urlUploadBtn', function() {
		if (jq('#url_image').val().trim()) {
			jq('.popup-close').trigger('click');
			command.showLoader();
			jq.ajax({
				url : opc_url_upload,
				type : "POST",
				data : {
					imgPath : jq('#url_image').val().trim()
				},
				success : function(data) {
					var n = '';
					n = data.slice(-6);
					if (n == '.Error') {
						data = data.replace('Error', '');
						alert(data);
						command.hideLoader();
					} else {
						command.showUploadedImages(data, 'img_userUpload');
					}
				},
				error : function() {
					command.hideLoader();
				}
			});
		} else
			alert('Please Enter Image URL Path')
	});

	/*
	 Add Image button event.
	 */

	jq(document).on('click', '#clipList li', function() {
		var src = jq(this).find('img').attr('src');
		var id = jq(this).find('img').attr('id');
		command.showUploadedImages(src, id);
		return false;

	});

	/*
	 Cancel button event in image panel
	 */

	jq(document).on(opc_EventType, '.img_lib_bck', function() {
		jq('.library-preview').css('display', 'none');
		jq('#browse_panel').css('display', 'block');
		return false;
	});

	/*
	 Go Back button and browse cancel button event.
	 */

	jq(document).on(opc_EventType, '.browse_bck, #prev_back', function() {

		command.manageInnerPanel('imagePanel');
		jq('.activeTab').removeClass('activeTab');
		return false;
	});

	jq(document).on(opc_EventType, '.product-cancel', function() {
		jq('#tool_panel, #upload_panel, .device-section').css('display', 'block');
		jq('.footer-toolbar').slideDown();
		jq('#product_panel').css('display', 'none');
		jq('[alt="product_tab"]').removeClass('activeTab');
		jq('html').addClass('html-box');
		return false;

	});

	jq(document).on(opc_EventType, '.delete-btn', function() {
		jq(this).parents('li').remove();
		return false;
	});

	/*
	 Layer Up/Down code.
	 */

	jq(document).on(opc_EventType, '.layer-order', function() {
		command.layerUpDown(jq(this));
		return false;
	});

	/*
	 Image Upload Event.
	 */

	jq(document).on(opc_EventType, '#uploadIMg li img', function() {
		command.showLoader();
		var src = jq(this).attr('src').replace('thumb', 'original');
		var clipId = jq(this).attr('id').split('_')[1];
		var clipPrice = 0;

		if (clipId == 'userUpload') {
			clipPrice = opc_upload_img_price;
		} else {
			jq.each(opc_Clipart_Price, function(key, value) {
				if (value.id == clipId) {
					clipPrice = value.price;
				}
			});
		}
		command.imgAdd(src, 'Image', parseFloat(clipPrice));
		jq('.currentImg').removeClass('currentImg');
		return false;
	});

	/*
	 View Change code
	 */

	jq(document).on(opc_EventType, '.select-device-view figure', function() {
		command.viewChange(jq(this));
		return false;
	});

	/*
	 Add text panel
	 */

	jq(document).on(opc_EventType, '#addText', function() {
		if (jq('#textP').val()) {
			text_obj_val.val = jq('#textP').val();
			text_obj_val.fontFamily = jq('.font').css('font-family');
			text_obj_val.color = jq('.color').find('i').css('background-color');
			text_obj_val.colorName = jq('.color').text().trim();
			text_obj_val.fontIndexId = jq('.font').attr('alt').split('font_')[1];
			command.addText()
		}
		return false;
	});

	jq(document).on(opc_EventType, '#updateText', function() {
		opc_GetObj = opc_canvas.getActiveObject();
		if (opc_GetObj && opc_GetObj.name == 'text') {
			opc_GetObj.setText(jq('#textP').val()).setCoords();
			command.saveStateModified();
		}
		return false;
	});

	
	/*
	 Bold and Italic
	 */

	jq(document).on(opc_EventType, '.text-tool span', function() {
		opc_GetObj = opc_canvas.getActiveObject();
		if (opc_GetObj && opc_GetObj.name == 'text') {
			jq(this).toggleClass('active');
			switch(jq(this).index()) {
				case 0 :
					jq(this).hasClass('active') ? opc_GetObj.setFontWeight('700').setCoords() : opc_GetObj.setFontWeight('normal').setCoords();
					break;
				case 1 :
					jq(this).hasClass('active') ? opc_GetObj.setFontStyle('italic').setCoords() : opc_GetObj.setFontStyle('').setCoords();
					break;
			}
			opc_canvas.renderAll().calcOffset();
		}
		return false;
	});

	/*
	 Object delete event.
	 */

	jq(document).on(opc_EventType, '#deleteObj', function() {
		opc_GetObj = opc_canvas.getActiveObject();
		if (opc_GetObj) {

			opc_canvas.remove(opc_GetObj).renderAll().calcOffset();
			command.saveStateModified();
		}
		return false;
	});

	/*
	 Font and color List dropdown.
	 */

	jq(document).on(opc_EventType, '.select-box', function() {
		if(jq(this).next('ul').hasClass('catDrop')){
			jq('ul.subcatDrop').slideUp();
		}else if(jq(this).next('ul').hasClass('subcatDrop')){
			jq('ul.catDrop').slideUp();
		}
		jq(this).next('ul').slideToggle(300);
		jq(this).parents('.custom-select').toggleClass('active-select');
		jq('.scroll').mCustomScrollbar("update");
		return false;
	});
	jq(document).on('click', '.dropdown li', function() {
		var currentText = jq(this).html();
		jq(this).parents('ul').siblings('.select-box').html(currentText)
		jq(this).parents('.dropdown').slideUp(300);

		if (jq(this).parents().hasClass('fontDrop')) {

			var fontFamily = jq(this).css('font-family');
			var fontIndex = jq(this).attr('alt');
			jq(this).parents('ul').siblings('.select-box').css('font-family', fontFamily).attr('alt', fontIndex);
			opc_GetObj = opc_canvas.getActiveObject();

			if (opc_GetObj && opc_GetObj.name == 'text') {
				opc_GetObj.set({
					fontFamily : fontFamily,
					fontIndexId : fontIndex.split('font_')[1]
				}).setCoords();
				opc_canvas.renderAll().calcOffset();
				command.saveStateModified();

			}

			var fontIndex = jq(this).attr('alt').split('_')[1];
			jq.each(WebFontConfig.google.families, function(key, value) {
				if (key == fontIndex) {
					value.match('700') ? jq('.text-tool span:eq(0)').removeClass('hide-panel') : jq('.text-tool span:eq(0)').addClass('hide-panel');
					value.match('italic') ? jq('.text-tool span:eq(1)').removeClass('hide-panel') : jq('.text-tool span:eq(1)').addClass('hide-panel');
				}
			});
		} else if (jq(this).parents().hasClass('colorDrop')) {
			var selectColor = jq(this).find('i').css('background-color');
			var selectColName = jq(this).text().trim();
			opc_GetObj = opc_canvas.getActiveObject();

			if (opc_GetObj && opc_GetObj.name == 'text') {
				opc_GetObj.set({
					fill : selectColor,
					txtColName : selectColName
				}).setCoords();
				opc_canvas.renderAll().calcOffset();
				command.saveStateModified();

			}

		}
		return false;

	});

	jq(document).on(opc_EventType, '.dropdown li', function() {
		var currentText = jq(this).html();
		jq(this).parents('ul').siblings('.select-box').html(currentText);
		jq(this).parents('ul').siblings('.select-box').attr('id', jq(this).attr('id').split('_')[1]);
		jq(this).parents('.dropdown').slideUp(300);
		if (jq(this).parents().hasClass('catDrop')) {
			var catId = jq(this).attr('id').split('_')[1];
			service.clipartManage(service.clipartData, 'catChange', catId, '');

		} else if (jq(this).parents().hasClass('subcatDrop')) {
			var catId = jq('#clipCategory .select-box').attr('id');
			var subcatId = jq(this).attr('id').split('_')[1];
			service.clipartManage(service.clipartData, 'subCatChange', catId, subcatId);
		}
		return false;
	});

	/*
	Colorpicker plugin function.
	*/

	
	jq('#canPicker').colpick({
		colorScheme : 'dark',
		layout : 'rgbhex',
		color : 'ffffff',
		height : 120,
		onSubmit : function(hsb, hex, rgb, el) {
			opc_canvas.setBackgroundColor('#' + hex, opc_canvas.renderAll.bind(opc_canvas));
			opc_backgroundColor[opc_current_view] = hex;
			command.saveStateModified();
			jq(el).colpickHide();
		}
	});

	/*
	 Rotate manage
	 */

	jq(document).on('mousedown', '#rotateThumb', function(event) {
		document.addEventListener('mouseup', myThumbUp, false);
		document.addEventListener('mousemove', myThumbMove, false);
		return false;
	});

	jq(document).on('touchstart', '#rotateThumb', function() {
		document.addEventListener('touchend', myThumbTouchUp, false);
		document.addEventListener('touchmove', myThumbTouchMove, false);
		return false;
	})

	opc_canvas.observe("object:rotating", rotateHandler);
	

	jq.fn.ForceNumericOnly = function() {
		return this.each(function() {
			jq(this).keydown(function(e) {
				var key = e.charCode || e.keyCode || 0;
				// allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
				// home, end, period, and numpad decimal
				return (key == 8 || key == 9 || key == 46 || key == 110 || key == 190 || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
			});
		});
	};

	jq('#rotateVal').ForceNumericOnly().keyup(function() {
		if (jq(this).val() < 0 || jq(this).val() > 360) {
			jq(this).val(0);
		}
	}).blur(function() {
		opc_GetObj = opc_canvas.getActiveObject();
		var val = jq(this).val();
		if (opc_GetObj) {
			jq(".nobe-svg").css({
				transform : 'rotate(' + (val / 2) + 'deg)'
			});
			jq('#rotateVal').val(Math.ceil(val));
			opc_GetObj.set("angle", Math.ceil(val)).setCoords();
			opc_canvas.renderAll().calcOffset();
		}
	});

	/*
	 Transparent silder code.
	 */

	jq('#tran_Slide').slider({
		min : 0,
		max : 100,
		value : 0,
		animate : true,
		slide : function(event, ui) {
			opc_GetObj = opc_canvas.getActiveObject();
			if (opc_GetObj) {
				opc_GetObj.setOpacity((ui.value / 100)).setCoords();
				opc_canvas.renderAll().calcOffset();
			}
		},
		stop : function() {
			command.saveStateModified();
		}
	}).draggable();

	/*
	 Object Zoom silder code.
	 */

	jq("#scaleSlide").slider({
		min : 1,
		value : 1,
		orientation : "horizontal",
		animate : true,
		max : 200,

		slide : function(event, ui) {
			opc_GetObj = opc_canvas.getActiveObject();
			if (opc_GetObj) {
				opc_GetObj.scale(ui.value / 100).setCoords();
				opc_canvas.renderAll().calcOffset();

			}
		},
		stop : function() {
			command.saveStateModified();
		}
	}).draggable();

	jq(document).on(opc_EventType, '.apply-effect h5', function() {

		command.effectPanel('show');

	})

	jq('#effectDrag').addClass('ui-draggable').css('position', 'absolute').draggable({
		containment : "div#wrapper",
		handle : "header h4"
	});
	jq(document).on(opc_EventType, '#effect-list li', function() {
		if (jq(this).hasClass('active-effect'))
			return false;

		opc_GetObj = opc_canvas.getActiveObject();
		if (opc_GetObj && opc_GetObj.type == 'image') {
			command.showLoader();
			jq('#effect-list li').removeClass('active-effect');
			jq(this).addClass('active-effect');
			var alt = jq(this).attr('alt');
			switch(alt) {
				case 'no-effect':
					effect_mod.Noeffect(false);
					opc_GetObj.set({
						sharpenEf : false,
						BlacknWhiteEf : false,
						sepiaEf : false
					})
					command.objRender();
					break;
				case 'sharpen':
					opc_GetObj.set('sharpenEf', true);
					effect_mod.Noeffect(false);
					effect_mod.Sharpen(true);
					break;
				case 'black-n-white':
					effect_mod.Noeffect(false);
					effect_mod.Grayscale(true);
					opc_GetObj.set('BlacknWhiteEf', true);
					break;
				case 'sepia':
					effect_mod.Noeffect(false);
					effect_mod.Sepia2(true);
					opc_GetObj.set('sepiaEf', true);
					break;
			}
			command.saveStateModified();
			setTimeout(function() {
				command.hideLoader();
			}, 500);
		}
		return false;
	});

	/*
	 Social Sharing button Event code.
	 */

	jq('body').on(opc_EventType, '.social-share li', function() {
		var target = jq(this).attr('data-value');

		setTimeout(function() {
			command.updatePreview();

			jq.ajax({
				url : opc_ShareImg,
				type : 'POST',
				async : false,
				data : {
					imgData : service.previewArr[opc_current_view]
				},
				success : function(path) {
					switch(target) {
						case 'twitter' :
							onTwitterShare(path);
							break;
						case 'facebook' :
							onFacebookShare(opc_ToolPath, path);
							break;
						case 'email' :
							onEmailShare(path);
							break;
						case 'rss' :
							break;
					}
				}
			});
		}, 350);
		return false;
	});

	jq('body').on(opc_EventType, '.undo-redo a', function() {
		if (jq(this).hasClass('undo')) {

			opc_stateIndex[opc_current_view]--;
			command.undo();
		} else {

			opc_stateIndex[opc_current_view]++;
			command.redo();
		}
		return false;
	});

	jq(document).on(opc_EventType, '.buy-now a', function() {
		command.addToCart();
	});

	jq(document).on(opc_EventType, '.lock-img', function() {
		jq(this).toggleClass('unlock');
		if (jq(this).hasClass('unlock')) {
			command.maintainRatioLock('off');
		} else {
			command.maintainRatioLock('on');
		}
		return false;
	});

})(jQuery);

function myThumbMove(event) {
	var get_obj = opc_canvas.getActiveObject();

	if (get_obj) {
		/******************************For Normal Case ****************/
		// var npx=event.clientX-jquery(".rotateBox").offset().left;
		// var npy=((event.clientY)-jquery(".rotateBox").offset().top);

		/**************************for scroll rotate ***************/
		var npx = event.pageX - jq(".rotateBox").offset().left;
		var npy = ((event.pageY) - jq(".rotateBox").offset().top);
		//	alert(npy)
		npx = 25 - npx;
		npy = 25 - npy;
		var angle = Math.atan2(npy, npx);
		//console.log(npx+"\n"+npy)
		//console.log(angle)
		angle = (angle * 180) / Math.PI;
		angle -= 180;
		var _angle = (angle < 0) ? 360 - (angle * -1) : angle;
		var nextX = 33 + Math.cos(_angle * Math.PI / 180) * radius;
		var nextY = 33 + Math.sin(_angle * Math.PI / 180) * radius;
		get_obj.originX = 'center';
		get_obj.originY = 'center';
		var boundingRect = get_obj.getBoundingRect();
		var nwc = boundingRect.left + (boundingRect.width / 2);
		var nhc = boundingRect.top + (boundingRect.height / 2);
		get_obj.set({
			left : nwc,
			top : nhc
		});
		opc_canvas.renderAll();
		get_obj.set("angle", Math.round(_angle));
		jq('#rotationVal').val(Math.round(_angle));
		get_obj.setCoords();
		opc_canvas.renderAll();
		opc_canvas.calcOffset();
		jq(".rotater").css({
			left : nextX,
			top : nextY
		});
	}
}

function myThumbTouchMove(event) {
	var get_obj = opc_canvas.getActiveObject();
	if (get_obj) {
		event.preventDefault();
		var touch = event.touches[0];
		/******************************For Normal Case ****************/
		// var npx=event.clientX-jquery(".rotateBox").offset().left;
		// var npy=((event.clientY)-jquery(".rotateBox").offset().top);

		/**************************for scroll rotate ***************/
		var npx = touch.pageX - jq(".rotateBox").offset().left;
		var npy = ((touch.pageY) - jq(".rotateBox").offset().top);
		//alert(npy)
		npx = 25 - npx;
		npy = 25 - npy;
		var angle = Math.atan2(npy, npx);
		//console.log(npx+"\n"+npy)
		//console.log(angle)
		angle = (angle * 180) / Math.PI;
		angle -= 180;
		var _angle = (angle < 0) ? 360 - (angle * -1) : angle;
		var nextX = 33 + Math.cos(_angle * Math.PI / 180) * radius;
		var nextY = 33 + Math.sin(_angle * Math.PI / 180) * radius;
		get_obj.originX = 'center';
		get_obj.originY = 'center';
		var boundingRect = get_obj.getBoundingRect();
		var nwc = boundingRect.left + (boundingRect.width / 2);
		var nhc = boundingRect.top + (boundingRect.height / 2);
		get_obj.set({
			left : nwc,
			top : nhc
		});
		opc_canvas.renderAll();
		get_obj.set("angle", Math.round(_angle));
		jq('#rotationVal').val(Math.round(_angle));
		opc_canvas.renderAll();
		jq(".rotater").css({
			left : nextX,
			top : nextY
		});
		//$(".rotateDegree").text(Math.round(_angle));

	}

}

function myThumbUp(event) {
	command.saveStateAdded();
	document.removeEventListener('mousemove', myThumbMove, false);
}

function myThumbTouchUp(event) {
	command.saveStateAdded();
	document.removeEventListener('touchmove', myThumbTouchMove, false);
}

function rotateHandler(event, value) {
	//console.log(event.target.getAngle());
	if (event) {
		var angle = event.target.getAngle();
	} else {
		var angle = value;
	}

	angle = (angle > 360) ? (angle - 360) : angle;

	var nextX = 33 + Math.cos(angle * Math.PI / 180) * radius;
	var nextY = 33 + Math.sin(angle * Math.PI / 180) * radius;

	jq(".rotater").css({
		left : nextX,
		top : nextY
	});
	jq('#rotationVal').val(Math.round(angle));
}



