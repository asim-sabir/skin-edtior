(function($) {

	/*
	 Product Manage
	 */

	jq.ajax({
		url : opc_product_json,
		type : "POST",
		success : function(data) {
			service.productData = data;
			service.productManage();
		}
	});

	service.productManage = function() {
		var productLi = '';

		jq.each(service.productData["products"], function(key, value) {
			productLi += '<li id="prod_' + value.id + '"><a href="javascript:void(0);"><figure><img alt="" src="' + value.path + '"><figcaption> ' + value.title + ' </figcaption></figure></a></li>';
		});

		jq("#productList").html(productLi).promise().done(function() {
			jq("#brand_panel").hide("slide", {
				direction : "left"
			}, "slow", function() {
				jq("#product_panel").show("slide", {
					direction : "left"
				}, "slow");
			});
		});

	}

	service.brandManage = function(scope) {

		var brandLi = '';
		service.prodId = scope ? scope.attr('id').split('prod_')[1] : service.prodId;

		jq.each(service.productData["products"], function(key, value) {
			if (service.prodId == value.id) {
				jq.each(value["brands"], function(key, value) {
					brandLi += '<li id="brand_' + value.id + '"><a href="javascript:void(0);"><figure><img alt="" src="' + value.path + '" ><figcaption> ' + value.title + ' </figcaption></figure></a></li>';
				});
			}
		})

		jq("#brandList").html(brandLi).promise().done(function() {
			if (scope) {
				jq("#product_panel").hide("slide", {
					direction : "right"
				}, "slow", function() {
					jq("#brand_panel").show("slide", {
						direction : "right"
					}, "slow");
				});
			} else {
				jq("#model_panel").hide("slide", {
					direction : "left"
				}, "slow", function() {
					jq("#brand_panel").show("slide", {
						direction : "left"
					}, "slow");
				});
			}
		});

	}

	service.modelManage = function(scope) {

		var modelLi = '';
		service.brandId = scope.attr('id').split('brand_')[1];

		jq.each(service.productData["products"], function(key, value) {
			if (service.prodId == value.id) {
				jq.each(value["brands"], function(key, value) {
					if (service.brandId == value.id) {
						jq.each(value["device"], function(key, value) {
							modelLi += '<li id="model_' + value.id + '"><a href="javascript:void(0);"><figure><img src="' + value.img + '"  alt="" /><figcaption> ' + value.model + ' </figcaption></figure></a></li>';
						});
					}
				});
			}
		});

		jq("#modelList").html(modelLi).promise().done(function() {
			jq("#brand_panel").hide("slide", {
				direction : "right"
			}, "slow", function() {
				jq("#model_panel").show("slide", {
					direction : "right"
				}, "slow");
			});
		});

	}

	service.viewLoad = function(scope) {
		service.modelId = scope.attr('id').split('model_')[1];
		var view_li = '<i class="effect"></i>';

		jq.each(service.productData["products"], function(key, value) {
			if (service.prodId == value.id) {
				jq.each(value["brands"], function(key, value) {
					if (service.brandId == value.id) {
						jq.each(value["device"], function(key, value) {
							if (service.modelId == value.id) {
								opc_productPrice = value.price;
								service.viewData = value['views']['view'];
								jq('#previewPanel span').addClass('hide-panel');
								service.previewArr = new Array();
								opc_current_view = 0;
								opc_View = new Array();
								jq.each(value['views']['view'], function(key, value) {
									opc_View.push({
										'path' : value.path,
										'width' : value.width,
										'height' : value.height,
										'id' : value.id,
										'name' : value.title,
										'drawX' : 1,
										'drawY' : 1,
										'drawWidth' : value.width,
										'drawHeight' : value.height

									});

									service.previewArr[key] = value.path.replace('thumb', 'large');
									if (key == 0) {
										jq('.preview-front').removeClass('hide-panel').find(' img').attr({
											src : value.path.replace('thumb', 'large'),
											id : 'prev_' + key
										});
										jq('.device-section img').attr('src', value.path.replace('thumb', 'large') + '?_=' + new Date().getTime());
									} else if (key == 1) {
										jq('.preview-back').removeClass('hide-panel').find(' img').attr({
											src : value.path.replace('thumb', 'large'),
											id : 'prev_' + key
										});
									}

									view_li += '<figure class="front-view" id="view_' + key + '"><figcaption> ' + value.title + ' </figcaption><div class="img-section"><img alt="" src="' + value.path + '"></div></figure>';
									opc_canvas_arr[key] = new Array();
								});
							}
						});
					}
				});
			}
		});
		jq('.device-section img').one("load", function() {
			command.canvasLoad(service.viewData[0]);
			opc_state = new Array();
			opc_stateIndex = new Array();
			opc_action = new Array();
			command.calculatePrice(opc_productPrice);
			command.hideLoader();
		});
		jq('.select-device-view').html(view_li).find('#view_0').addClass('active-view');
		jq('.tool-pallet li').removeClass('activeTab');

		jq('.pre-tool-panel').css('display', 'none');
		jq('.model-table-outer, #upload_panel, .device-section').css('display', 'block').promise().done(function() {
			jq('.footer-toolbar').removeClass('hide-panel').slideDown();
		});

	}
	/*
	 Clipart Manage
	 */

	jq.ajax({
		url : opc_clipart_json,
		type : "POST",
		success : function(data) {
			service.clipartData = data;
			service.clipartManage(service.clipartData, 'default', '', '');
		}
	});

	service.clipartManage = function(json, type, isDefaultCat, isDefaultSubCat) {
		var totalLength = '', catName = '', catId = '', catLi = '', subCatName = '', subCatId = '', subCatLi = '', clipId = '', clipName = '', clipPath = '', isDefaultCat = isDefaultCat, isDefaultSubCat = isDefaultSubCat, firstId = '', firstName = '';
		clipLi = '';
		var clip_li = '';

		jq.each(json["category"], function(key, value) {

			catName = value.title;
			catId = value.id;

			isDefaultCat = ((type == 'default') ? value.isDefault : isDefaultCat);
			if (catId == isDefaultCat)
				catLi += '<span class="select-box" id="' + catId + '">' + catName + '</span><ul class="dropdown scroll catDrop"><li id="cat_' + catId + '">' + catName + '</li>';
			else
				catLi += '<li id="cat_' + catId + '">' + catName + '</li>';

			if (catId == isDefaultCat) {
				jq.each(value.subcategory, function(key, value) {
					subCatName = value.title;
					subCatId = value.id;
					var c = 1;

					if (type == 'default' || type == 'catChange')
						isDefaultSubCat = value.isDefault;

					if (subCatId == isDefaultSubCat)
						subCatLi += '<span class="select-box" id="' + subCatId + '">' + subCatName + '</span><ul class="dropdown scroll subcatDrop"><li id="subcat_' + subCatId + '">' + subCatName + '</li>';
					else
						subCatLi += '<li id="subcat_' + subCatId + '">' + subCatName + '</li>';
					if (subCatId == isDefaultSubCat) {

						jq.each(value.pattern, function(key, value) {
							clipName = value.title;
							clipPath = value.thumb;
							clipId = value.id;
							opc_Clipart_Price.push({
								"id" : clipId,
								"price" : value.price
							});
							clipLi += '<li><span><img id="clip_' + clipId + '" alt="" src="' + clipPath + '"></span><span class="caption"> ' + clipName + ' </span></li>';
							c++;
						});
					}

				});
				subCatLi += '</ul>';
			}

		});
		catLi += '</ul>';
		if (type == 'default') {
			jq('#clipCategory').html(catLi).promise().done(function() {
				jq(".catDrop").mCustomScrollbar({
					theme : "minimal"
				});
			});
			jq('#clipSubcategory').html(subCatLi).promise().done(function() {
				jq(".subcatDrop").mCustomScrollbar({
					theme : "minimal"
				});
			});
		} else if (type == 'catChange') {
			jq('#clipSubcategory').html(subCatLi).promise().done(function() {
				jq(".subcatDrop").mCustomScrollbar({
					theme : "minimal"
				});
			});
		}

		jq('#clipList .mCSB_container').html(clipLi);
		//jq('#clipList li > span').ImgZoom();

		jq('#uploadIMg').mCustomScrollbar({
			theme : "minimal"
		});
	}
	/*
	 Font Load  Section
	 */

	jq.each(WebFontConfig.google.families, function(key, value) {
		if (key == 0) {
			opc_Font_List += '<span alt="font_' + key + '" class="select-box font" style="font-family : ' + value.split(':')[0].replace(/\+/g, ' ') + ' ">' + value.split(':')[0].replace(/\+/g, ' ') + '</span><ul class="dropdown scroll fontDrop"><li alt="font_' + key + '" style="font-family : ' + value.split(':')[0].replace(/\+/g, ' ') + ' ">' + value.split(':')[0].replace(/\+/g, ' ') + '</li>';
			value.match('700') ? jq('.text-tool span:eq(0)').removeClass('hide-panel') : jq('.text-tool span:eq(0)').addClass('hide-panel');
			value.match('italic') ? jq('.text-tool span:eq(1)').removeClass('hide-panel') : jq('.text-tool span:eq(1)').addClass('hide-panel');
		} else
			opc_Font_List += '<li alt="font_' + key + '" style="font-family : ' + value.split(':')[0].replace(/\+/g, ' ') + ' ">' + value.split(':')[0].replace(/\+/g, ' ') + '</li>';
	});
	opc_Font_List += '</ul>';
	jq('#fontList').html(opc_Font_List).promise().done(function() {
		jq(".fontDrop").mCustomScrollbar({
			theme : "minimal"
		});
	});

	/*
	 Font Color load
	 */

	jq.ajax({
		url : opc_color_json,
		type : "POST",
		success : function(data) {
			service.colorData = data;
			service.colorManage();
		}
	})

	service.colorManage = function() {
		var opc_colorList = '';
		jq.each(service.colorData, function(key, value) {
			if (key == 0)
				opc_colorList += '<span class="select-box color"> <i class="color-pallet" style="background-color:' + value.color + ';"></i> ' + value.colorName + ' </span><ul class="dropdown scroll colorDrop"><li id="col_' + key + '"><i class="color-pallet" style="background-color:' + value.color + ';"></i> ' + value.colorName + ' </li>';
			else
				opc_colorList += '<li id="col_' + key + '"><i class="color-pallet" style="background-color:' + value.color + ';"></i> ' + value.colorName + ' </li>';
		});
		opc_colorList += '</ul>';
		jq('#colorList').html(opc_colorList).promise().done(function() {
			jq(".colorDrop").mCustomScrollbar({
				theme : "minimal"
			});
		});
	}
})(jQuery);
