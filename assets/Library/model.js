/*
Variable Name :
 1. command.can_wid 
 2. command.can_heig
 3. command.img_left
 4. command.img_top
 5. command.img_wid
 6. command.img_heig 
 7. text_obj_val.val
 8. text_obj_val.fontFamily
 9. text_obj_val.color
 10. text_obj_val.colorName
 
 
 Function Name :
 1. command.canvasLoad function is used in command.js file for managing canvas rendering code.
 2. command.viewChange function is used in command.js file for managing view change code.
 3. command.imgAdd function is used in command.js file for adding a image in canvas.
 4. command.addText function is used in command.js file for add text in canvas.
 5. command.showLoader and command.hideLoader are used for loader manage code.
 6. service.productManage function is used for loading a product images.
 7. service.productData variable is used for cantaining a product json scope.
 8. service.prodId variable is used for cantaining product id.
 9. service.brandId variable is used for cantaining brand id.
 10. service.modelId variable is used for cantaining model id.
 11. service.viewData object( for cantaining view data )
 12. service.previewArr
 13. service.brandManage function is used for loading a brand images.
 14. service.modelManage function is used for loading model images.
 15. service.viewLoad function is used for loading view images.
 16. service.clipartManage function is used for loading clipart images.
 17. service.clipartData variable
 18. service.colorManage function is used for loading a color.
 19. service.colorData variable(used for taking color json)

*/
/* Global Constants */
var opc_text_price = 1;
var opc_upload_img_price = 2;
var opc_currency = '$';
var opc_show_adjustment_window = true;//  true or false

/*
 Global configuration for Upload Image panel.
 */
var opc_Max_file_size = '5mb';
var opc_Upload_File_Extension = 'png,jpg,jpeg,gif,svg';
var opc_Chunk_Image_Size = '50kb';
var opc_Upload_Imgfile_path = 'assets/PHP/upload.php';
var opc_UploadImg_Multi_Selection = false;//  true or false
var opc_UploadImg_Unique_Names = true;//  true or false
/*
 PHP file Path.
 */
/*var opc_url_upload = "assets/PHP/LIFU.php";
var opc_ShareImg = 'assets/PHP/share.php';
var opc_ToolPath = 'http://norefresh.thesparxitsolutions.com/Shahnawaz/SkinDesignerTool/LivePreview/';
var opc_Cart_URL = 'outputDataView.php';*/


var jq = $.noConflict();
var opc_canvas = '';
var opc_current_view = 0;
var opc_canvas_arr = new Array();
var opc_GetObj = '';
var radius = 41.5;

/*
 JSON file Path.
 */

var opc_product_json = "assets/JSON/product.json";
var opc_clipart_json = "assets/JSON/pattern.json";
var opc_color_json = "assets/JSON/color.json";


var command = {};
var text_obj_val = {};
var service = {};


var opc_EventType = 'ontouchend' in document ? 'touchend' : 'click';

var opc_isIE = /*@cc_on!@*/false || !!document.documentMode;



/*
 Global configuration for Handler Icons.
 */
var opc_Top_RightImg = new Image();
opc_Top_RightImg.src = 'assets/Library/Handler_Image/rotate-clipart.png';
var opc_Left_Resize = new Image();
opc_Left_Resize.src = 'assets/Library/Handler_Image/close-clipart.png';
var opc_Right_Resize = new Image();
opc_Right_Resize.src = "assets/Library/Handler_Image/bottom-left-resizer.png";
var opc_Bottom_up_Resize = new Image();
opc_Bottom_up_Resize.src = "assets/Library/Handler_Image/botom-up-resize.png";
var opc_Left_Right_Resize = new Image();
opc_Left_Right_Resize.src = "assets/Library/Handler_Image/left-right.png";
var opc_Rotate = new Image();
opc_Rotate.src = "assets/Library/Handler_Image/rotate-clipart.png";
var opc_Bottm_RightImg = new Image();
opc_Bottm_RightImg.src = 'assets/Library/Handler_Image/bottom-right-resizer.png';
var opc_Rotate_Cursor = 'assets/Library/Handler_Image/Rotate.cur';


/*
 Variable Name
*/
var opc_Font_List = '';
var opc_state = new Array();
var opc_stateIndex = new Array();
var opc_action = new Array();
var opc_textInputInterval;
var opc_Output_SVG = new Array();
var opc_png_Output = new Array();
var opc_RatioX = 1;
var opc_RatioY = 1;
var opc_backgroundColor = new Array();
var opc_View;
var opc_extendedWidth;
var opc_extendedHeight;
var opc_requiredSpace = 100;
var opc_totalPrice;
var opc_quantity = 1;
var opc_productPrice;
var isScaling = false;
var opc_Clipart_Price = new Array();
var opc_GetPrice = 0;
var opc_Current_View_Price = 0;

