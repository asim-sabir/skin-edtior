var effect_mod = (function() {
	function applyFilter(index, filter) {
		var obj = opc_canvas.getActiveObject();
		if (obj) {
			obj.filters[index] = filter;
			obj.applyFilters(opc_canvas.renderAll.bind(opc_canvas));
		}
	}

	function applyFilterValue(index, prop, value) {
		var obj = opc_canvas.getActiveObject();
		if (obj.filters[index]) {
			obj.filters[index][prop] = value;
			obj.applyFilters(opc_canvas.renderAll.bind(opc_canvas));
		}
	}

	return {
		Invert : function(flag) {
			applyFilter(1, flag && new f.Invert());
		},
		Sepia : function(flag) {
			applyFilter(2, flag && new f.Sepia());
		},
		Sepia2 : function(flag) {
			applyFilter(3, flag && new f.Sepia2());
		},
		Grayscale : function(flag) {
			applyFilter(4, flag && new f.Grayscale());
		},
		RemoveWhite : function(flag) {
			applyFilter(5, flag && new f.RemoveWhite({
				threshold : 220,
				distance : 150
			}));
		},
		Brightness : function(flag) {
			applyFilter(6, flag && new f.Brightness({
				brightness : parseInt(150, 10)
			}));
		},
		Noise : function(flag) {
			applyFilter(7, flag && new f.Noise({
				noise : parseInt(150, 10)
			}));
		},
		Pixelate : function(flag) {
			applyFilter(8, flag && new f.Pixelate({
				blocksize : parseInt(150, 10)
			}));
		},
		Tint : function(flag, color) {
			applyFilter(10, flag && new f.Tint({
				color : color,
				opacity : parseFloat(0.5)
			}));
		},
		Multiply : function(flag) {
			applyFilter(11, flag && new f.Multiply({
				color : 'green'
			}));
		},
		Sharpen : function(flag) {
			applyFilter(12, flag && new f.Convolute({
				matrix : [0, -1, 0, -1, 5, -1, 0, -1, 0]
			}));
		},
		Noeffect : function(flag) {
			effect_mod.Sharpen(flag);
			effect_mod.Sepia2(flag);
			effect_mod.Grayscale(flag);
		}
	}

})();
